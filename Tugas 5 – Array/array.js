// //Soal No. 1 (Range) 

function range(startNum, finishNum) {
	var state = [];
	if (startNum < finishNum) {
		for (var i = startNum; i <= finishNum; i++) {
			state.push(i);
		}
	}
	else if(startNum > finishNum){
		for (var i = startNum; i >= finishNum; i--) {
			state.push(i);
		}
	}
	else if(startNum == null || finishNum == null){
		state.push(-1)
	}
	return state;
}
console.log(range(1,10));
console.log(range(1));
console.log(range(11,18));
console.log(range(54,50));
console.log(range());

//Soal No. 2 (Range with Step)
function rangeWithStep(startNum, finishNum, step) {
	var state = [];
	if (startNum < finishNum) {
		for (var i = startNum; i <= finishNum; i++) {
			state.push(i);
			i+=step-1;
		}
		
	}
	else if(startNum > finishNum){
		for (var i = startNum; i >= finishNum; i--) {
			state.push(i);
			i-=step-1;
		}
		
	}
	else if(startNum == null || finishNum == null){
		state.push(-1);
	}
	return state;
}
console.log(rangeWithStep(1, 10, 2))
console.log(rangeWithStep(11, 23, 3)) 
console.log(rangeWithStep(5, 2, 1)) 
console.log(rangeWithStep(29, 2, 4))

//Soal No. 3 (Sum of Range)
function sum(startNum, finishNum, step) {
	if (startNum == null ) return 0;	
	if (finishNum == null) return startNum;
	if (step == null) step = 1;
	var state = rangeWithStep(startNum, finishNum, step);
	var total = 0;
	for (var i = 0; i < state.length; i++) {
		total = total + state[i];	
	}
	return total;
}
console.log(sum(1,10)) // 55
console.log(sum(5, 50, 2)) // 621
console.log(sum(15,10)) // 75
console.log(sum(20, 10, 2)) // 90
console.log(sum(1)) // 1
console.log(sum()) // 0 

//Soal No. 4 (Array Multidimensi)
console.log("Soal No. 4 (Array Multidimensi)")

function dataHandiling(x){
	var input = [
                ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
                ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
                ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
                ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
            ] 
    if (x == 1) {
    	return "Nomor ID: " + input[0][1] + "\n" + "Nama Lengkap: " + input[0][2] + "\n" + "TTL: " + input[0][3] + "\n" + "Hobi: " + input[0][4];
    }else if(x == 2){
    	return "Nomor ID: " + input[1][1] + "\n" + "Nama Lengkap: " + input[1][2] + "\n" + "TTL: " + input[1][3] + "\n" + "Hobi: " + input[1][4];
    }else if(x == 3){
     	return "Nomor ID: " + input[2][1] + "\n" + "Nama Lengkap: " + input[2][2] + "\n" + "TTL: " + input[2][3] + "\n" + "Hobi: " + input[2][4];
    }else if(x == 4){
    	return "Nomor ID: " + input[3][1] + "\n" + "Nama Lengkap: " + input[3][2] + "\n" + "TTL: " + input[3][3] + "\n" + "Hobi: " + input[3][4];
    }
}
console.log(dataHandiling(1));
console.log("\n");
console.log(dataHandiling(2));
console.log("\n");
console.log(dataHandiling(3));
console.log("\n");
console.log(dataHandiling(4));

//Soal No. 5 (Balik Kata)
function balikKata(kata) {
var result = "";
for (var i = kata.length - 1; i >= 0; i--) {
	result = result + kata[i];
}
return result;
}
console.log(balikKata("Kasur Rusak")) // kasuR rusaK
console.log(balikKata("SanberCode")) // edoCrebnaS
console.log(balikKata("Haji Ijah")) // hajI ijaH
console.log(balikKata("racecar")) // racecar
console.log(balikKata("I am Sanbers"))


//Soal No. 6 (Metode Array)
function stringSplice(string, separator, startIndex, insert){
string = string.split(separator);
string.splice(startIndex,0,insert);
string = string.join(separator);
return string;
}
function dataHandling2(){
	var input = ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"];
	input[1] = stringSplice(input[1]," ",2,"Elsharawy");
	input[2] = stringSplice(input[2]," ",0,"Provinsi");
	input.splice(4,1,"Pria", "SMA Internasional Metro");

	var date = input[3].split("/");
	switch(date[1]){
		case "01" : date[1]= "Januari";break;
		case "02" : date[1]= "Februari";break;
		case "03" : date[1]= "Maret";break;
		case "04" : date[1]= "April";break;
		case "05" : date[1]= "Mei";break;
		case "06" : date[1]= "Juni";break;
		case "07" : date[1]= "Juli";break;
		case "08" : date[1]= "Agustus";break;
		case "09" : date[1]= "September";break;
		case "10" : date[1]= "Oktober";break;
		case "11" : date[1]= "November";break;
		case "12" : date[1]= "Desember";break;
	}
	var formattedDate = date.join("-");

	var sortedDate = input[3].split("/").sort(function(a,b){return parseInt(b)-parseInt(a)});
	var limitName = input[1].slice(0,15);
	console.log(input);
	console.log(date[1]);
	console.log(sortedDate);
	console.log(formattedDate);
	console.log(limitName);


}
dataHandling2();

