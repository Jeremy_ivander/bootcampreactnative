// Soal No. 1 (If-else)
var nama = "Brian"
var peran = "Guard"

if (nama == "") {
	console.log("nama harus diisi!");
}else if(peran == ""){
	console.log("Halo "+ nama +", Pilih peranmu untuk memulai game!");
}else if(peran == "Penyihir"){
	console.log("Halo "+ peran +' '+ nama +", kamu dapat melihat siapa yang menjadi werewolf!");
}else if(peran == "Guard"){
	console.log("Halo "+ peran +' '+ nama +", kamu akan membantu melindungi temanmu dari serangan werewolf!");
}else if(peran == "Werewolf"){
	console.log("Halo "+ peran +' '+ nama +", Kamu akan memakan mangsa setiap malam!");
}else{
	console.log("Maaf " + nama + ", peran tidak tersedia");


// Soal No. 2 (Switch Case)
var Tanggal = 9; 
var Bulan = 1; 
var Tahun = 1999; 

switch(Bulan) {
	case 1:{Bulan = 'Januari'; break;}
	case 2:{Bulan = 'Februari'; break;}
	case 3:{Bulan = 'Maret'; break;}
	case 4:{Bulan = 'April'; break;}
	case 5:{Bulan = 'Mei'; break;}
	case 6:{Bulan = 'Juni'; break;}
	case 7:{Bulan = 'Juli'; break;}
	case 8:{Bulan = 'Agustus'; break;}
	case 9:{Bulan = 'September'; break;}
	case 10:{Bulan = 'Oktober'; break;}
	case 11:{Bulan = 'November'; break;}
	case 12:{Bulan = 'Desember'; break;}

}
console.log(Tanggal+' '+Bulan+' '+Tahun);

// berikan "//" di soal 1 agar soal no 2 berfungsi