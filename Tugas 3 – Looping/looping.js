//No. 1 Looping While
// LOOPING 1
var angka = 0;
while(angka < 20){
	angka += 2;
	console.log(angka + ' - I love coding');
}
// LOOPING 2
var number = 22;
while(number <= 22 && number!=2){
	number -= 2;
	console.log(number + ' - I will become a mobile developer');
}

//No. 2 Looping for
for (var i = 0; i <= 20; i++) {
	if (i%2 == 0) {
		console.log(i + ' - Berkualitas')
	}
	else if (i%3 == 0){
		console.log(i + ' - I love coding')
	}
	else if(i%3 !== 0){
		console.log(i + ' - Santai')
	}
}

//No. 3 Membuat Persegi Panjang # 
var x ="";
for (var i = 0; i <= 4; i++) {
	for (var j = 1; j < 9; j++) {
		x +='#';
	}
	x +='\n';
}
console.log(x);

//No. 4 Membuat Tangga 
var e = '';	
for (var i = 0; i < 8; i++) {
	for (var j = 0; j < i; j++) {
		e+= '*';
	}
	e += '\n';
}
console.log(e)

// No. 5 Membuat Papan Catur
var b = "";

for (var y = 0; y < 8; y++) {  
  for (var x = 0; x < 8; x++) {
    if ((x + y) % 2 == 0)
      b += " ";
    else
      b += "#";
  }
  b += "\n";
}

console.log(b);